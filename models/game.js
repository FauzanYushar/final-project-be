'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GAME extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  GAME.init({
    name: DataTypes.STRING,
    cover: DataTypes.STRING,
    detail: DataTypes.STRING,
    guide: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'GAME',
  });
  return GAME;
};