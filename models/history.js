"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.History.belongsTo(models.USER, {
        foreignKey: "user_id",
        as: "user",
      }),
        models.History.belongsTo(models.GAME, {
          foreignKey: "game_id",
          as: "game",
        });
    }
  }
  History.init(
    {
      user_id: DataTypes.INTEGER,
      game_id: DataTypes.INTEGER,
      result: DataTypes.STRING,
      point: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "History",
    }
  );
  return History;
};
