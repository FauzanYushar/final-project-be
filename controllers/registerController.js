const { USER } = require("../models");

exports.submit = (req, res) => {
  USER.register(req.body)
    .then((data) => {
      res.status(200).json({ message: "Register success", data: data });
    })
    .catch((err) => {
      res.status(400).json({ message: err });
    });
};
